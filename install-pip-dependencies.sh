#!/bin/sh

# Root check 
if [ "$(id -u)" == "0" ]; then
	echo "This script must be run as a regular user" 1>&2
	exit 1
fi

pip3 install --user gpiozero pygame pyttsx3 SpeechRecognition
