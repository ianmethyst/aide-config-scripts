#!/bin/bash

# Root check 
if [ "$(id -u)" != "0" ]; then
	echo "This script must be run as root" 1>&2
	exit 1
fi

# Enable SPI

dtparam spi=on
sed -i '/spi\=on/s/^#//g' /boot/config.txt 


# Add modules

cat >> /etc/modules <<EOF
spi-bcm2835
fbtft_device
EOF

# Config fbtft_device
cat > /etc/modprobe.d/fbtft.conf <<EOF
options fbtft_device name=fb_ili9163 gpios=reset:25,dc:24,led:23 speed=40000000 bgr=1 custom=1 fps=60
EOF

