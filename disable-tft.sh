#!/bin/bash

# Root check 
if [ "$(id -u)" != "0" ]; then
	echo "This script must be run as root" 1>&2
	exit 1
fi

sed -i '/hdmi_force_hotplug=1/d' /boot/config.txt
sed -i '/hdmi_group=2/d' /boot/config.txt
sed -i '/hdmi_mode=87/d' /boot/config.txt
sed -i '/hdmi_cvt=128 128 60/d' /boot/config.txt
