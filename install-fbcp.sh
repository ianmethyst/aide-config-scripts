#!/bin/bash

# Root check 
if [ "$(id -u)" != "0" ]; then
	echo "This script must be run as root" 1>&2
	exit 1
fi

# Install cmake and git dependencies
apt install -y cmake git

# Clone repository
git clone https://github.com/tasanakorn/rpi-fbcp /tmp/rpi-fbcp

# Build rpi-fbcp
cd /tmp/rpi-fbcp
mkdir build
cd build
cmake ..
make

# Make it executable
chmod +x fbcp

# Copy to /usr/bin
cp fbcp /usr/bin

# Create systemd unit

cat > /lib/systemd/system/fbcp.service <<EOF
[Unit]
Description=fbcp service
After=multi-user.target

[Service]
Type=simple
ExecStart=/usr/bin/fbcp

[Install]
WantedBy=multi-user.target
EOF

# Restart systemd daemon
systemctl daemon-reload

# Enable and start unit
systemctl enable --now fbcp.service

# Cleanup
rm -rf /tmp/rpi-fbcp
