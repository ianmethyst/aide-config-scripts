#!/bin/bash

# Root check 
if [ "$(id -u)" != "0" ]; then
	echo "This script must be run as root" 1>&2
	exit 1
fi

cat >> /boot/config.txt <<EOF
hdmi_force_hotplug=1
hdmi_group=2
hdmi_mode=87
hdmi_cvt=128 128 60
EOF
