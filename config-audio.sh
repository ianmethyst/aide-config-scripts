#!/bin/bash

# Root check 
if [ "$(id -u)" != "0" ]; then
	echo "This script must be run as root" 1>&2
	exit 1
fi

# Disable Raspberry soundcard
cat >> /boot/config.txt <<EOF
audio_pwm_mode=2
dtparam=audio=off
EOF

# Change ALSA USB soundcard index
cat >> /etc/modprobed/alsa-base.conf <<EOF
options snd_usb_audio index=0
EOF
